/*
	Abstraction is like creating a simplified model or blueprint. It helps us focus on what's important and ignore the complicated details.
*/

// This class will serve as a blueprint for Employee class
class Person{
	constructor(){
		if(this.constructor === Person){
			throw new Error(
				"Object cannot be created from an abstract class Person"
			);
		}
		if(this.getFullName === undefined){
			throw new Error(
				"Class must implement getFullName() method."
			);
		}
		if(this.getFirstName === undefined){
			throw new Error(
				"Class must implement getFirstName() method."
			);
		}
		if(this.getLastName === undefined){
			throw new Error(
				"Class must implement getLastName() method."
			);
		}
	}
}

class Employee extends Person{
	constructor(firstName, lastName, employeeID){
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.employeeID = employeeID;
	}

	getFullName(){
		return `${this.firstName} ${this.lastName} has employee ID of ${this.employeeID}`;
	}

	getFirstName(){
		return `First Name: ${this.firstName}`;
	}

	getLastName(){
		return `Last Name: ${this.lastName}`;
	}
}

const employeeA = new Employee('John', 'Smith', 'EM-004');
console.log(employeeA.getFirstName());
console.log(employeeA.getLastName());
console.log(employeeA.getFullName());


class Student extends Person{
	constructor(firstName, lastName, studentID, section){
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.studentID = studentID;
		this.section = section;
	}

	getFullName(){
		return `${this.firstName} ${this.lastName} has student ID of ${this.studentID}`;
	}

	getFirstName(){
		return `First Name: ${this.firstName}`;
	}

	getLastName(){
		return `Last Name: ${this.lastName}`;
	}

	// Additional method
	getStudentDetails(){
		return `${this.firstName} ${this.lastName} has student ID of ${this.studentID} belongs to section ${this.section}`;
	}
}

const student1 = new Student('Brandon', 'Boyd', 'SID-1100', 'Mango');
const student2 = new Student('Jeff', 'Cruz', 'SID-1101', 'Narra');
const student3 = new Student('Jane', 'Doe', 'SID-1102', 'Sampaguita');

console.log(student1.getStudentDetails());
console.log(student2.getStudentDetails());
console.log(student3.getStudentDetails());
