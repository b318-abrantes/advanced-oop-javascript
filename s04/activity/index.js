// S04 Activity Instructions:

// 1.	Modify the RegularShape base class and the Triangle and Square subclasses from the previous session to encapsulate all their properties.

class RegularShape{
	constructor(){
		if(this.constructor === RegularShape){
			throw new Error(
				"You are not allowed to generate object in RegularShape class"
			);
		}
		if(this.getPerimeter === undefined){
			throw new Error(
				"Class must implement getPerimeter() method."
			);
		}
		if(this.getArea === undefined){
			throw new Error(
				"Class must implement getArea() method."
			);
		}
	}
}

class Square extends RegularShape{
	#noSides;
	#length;
	constructor(noSides, length){
		super();
		this.#noSides = noSides;
		this.#length = length;
	}

	getPerimeter(){
		return `The perimeter is ${this.#length*this.#noSides}`;
	}
	getArea(){
		return `The area is ${Math.pow(this.#length, 2)}`;
	}
	getLength(){
		return this.#length;
	}

	setNoSides(noSides){
		return this.#noSides = noSides;
	}
	setLength(length){
		return this.#length = length;
	}
}

class Triangle extends RegularShape{
	#noSides;
	#length;
	constructor(noSides, length){
		super();
		this.#noSides = noSides;
		this.#length = length;
	}

	getPerimeter(){
		return `The perimeter is ${this.#length*this.#noSides}`;
	}
	getArea(){
		return `The area is ${Math.round(((Math.sqrt(3)*Math.pow(this.#length, 2))/4)*1000)/1000}`;
	}
	getLength(){
		return this.#length;
	}

	setNoSides(noSides){
		return this.#noSides = noSides;
	}
	setLength(length){
		return this.#length = length;
	}
}

// Test Input and Expected Output:
const square1 = new Square();
square1.setNoSides(4);
square1.setLength(65);
console.log(square1.getLength()); // 65
console.log(square1.getPerimeter()); // The perimeter of the square is 260

const triangle1 = new Triangle();
triangle1.setNoSides(3);
triangle1.setLength(15);
console.log(triangle1.getLength()); // 15
console.log(triangle1.getArea()); // The area of the triangle is 97.428

/*
	2.	Create a base class called User with an empty constructor and is an abstract class that **requires** the following methods: 
			login
			register
			logout
		Create a subclass of User called RegularUser with the following properties:
			name
			email
			password
		and methods:
			login - returns "<name> has logged in."
			register - returns "<name> has registered."
			logout - returns "<name> has logged out"
			browseJobs - returns "There are 10 jobs found"
		Create another subclass of User called Admin with the following properties:
			name
			email
			password
			hasAdminExpired
		and methods:
			login - returns "Admin <name> has logged in."
			register - returns "Admin <name> has registered"
			logout - returns "Admin <name> has logged out"
			postJob - returns "Job posting added to site"
		NOTE: Don't forget to create the setter and getter methods
*/

class User {
	constructor() {
		if (this.constructor === User) {
		  throw new Error("Object cannot be created from an abstract class User");
		}
		if (this.login === undefined) {
		  throw new Error("Class must implement login() method");
		}
		if (this.register === undefined) {
		  throw new Error("Class must implement register() method");
		}
		if (this.logout === undefined) {
		  throw new Error("Class must implement logout() method");
		}
	}
}

class RegularUser extends User {
	#name;
	#email;
	#password;
	constructor(name, email, password) {
		super();
		this.#name = name;
		this.#email = email;
		this.#password = password;
	}
	login() {
		return `${this.#name} has logged in.`;
	}
	register() {
		return `${this.#name} has registered.`;
	}
	logout() {
		return `${this.#name} has logged out`;
	}
	browseJobs() {
		return "There are 10 jobs found";
	}


	getName() {
		return this.#name;
	}
	getEmail() {
		return this.#email;
	}
	getPassword() {
		return this.#password;
	}


	setName(name) {
		this.#name = name;
	}
	setEmail(email) {
		this.#email = email;
	}
	setPassword(password) {
		this.#password = password;
	}
}

const regUser1 = new RegularUser();
regUser1.setName("Dan");
regUser1.setEmail("dan@mail.com");
regUser1.setPassword("Dan12345");
console.log(regUser1.register()); // Dan has registered
console.log(regUser1.login()); // Dan has logged in
console.log(regUser1.browseJobs()); // There are 10 jobs found.
console.log(regUser1.logout()); // Dan has logged out


class Admin extends User {
	#name;
	#email;
	#password;
	#hasAdminExpired;
	constructor(name, email, password, hasAdminExpired) {
		super();
		this.#name = name;
		this.#email = email;
		this.#password = password;
		this.#hasAdminExpired = hasAdminExpired;
	}

	login() {
		return `Admin ${this.#name} has logged in.`;
	}
	register() {
		return `Admin ${this.#name} has registered.`;
	}
	logout() {
		return `Admin ${this.#name} has logged out.`;
	}
	postJob() {
		return `Job posting added to site.`;
	}

	getName() {
		return this.#name;
	}
	getEmail() {
		return this.#email;
	}
	getPassword() {
		return this.password;
	}
	getHasAdminExpired() {
		return this.#hasAdminExpired;
	}

	setName(name) {
		this.#name = name;
	}
	setEmail(email) {
		this.#email = email;
	}
	setPassword(password) {
		this.#password = password;
	}
	setHasAdminExpired(hasAdminExpired) {
		this.#hasAdminExpired = hasAdminExpired;
	}
}

const admin = new Admin();
admin.setName("Joe");
admin.setEmail("admin_joe@mail.com");
admin.setPassword("joe12345");
admin.setHasAdminExpired("false");
console.log(admin.register()); // Admin Joe has registered
console.log(admin.login()); // Admin Joe has logged in
console.log(admin.getHasAdminExpired()); // false
console.log(admin.postJob()); // Job added to site
console.log(admin.logout()); // Admin Joe has logged out