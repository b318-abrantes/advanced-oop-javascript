class Person{
	constructor(){
		if(this.constructor === Person){
			throw new Error(
				"You are not allowed to generate object in Person class"
			);
			
		}
		if(this.getFullName === undefined){
			throw new Error(
				"Class must implement getFullName() method."
			);
		}	
	}
}


class Employee extends Person {

	// Encapsulation, aided by private fields (#), ensures data protection. Setters and getters provide controlled access to encapsulated data.

	/* private fields */
	#firstName;
	#lastName;
	#employeeID;
	#password;

	constructor(firstName, lastName, employeeID){
		super();
		this.#firstName = firstName;
		this.#lastName = lastName;
		this.#employeeID = employeeID;

		// Additional example:
		// this.#password = password;
	}

	// getter methods
	getFirstName(){
		return `First Name: ${this.#firstName}`;
	}

	getLastName(){
		return `Last Name: ${this.#lastName}`;
	}

	getEmployeeId(){
		return this.#employeeID;
	}

	getFullName(){
		return `${this.#firstName} ${this.#lastName} has employee ID of ${this.#employeeID}`;
	}


	// setter methods 
	setFirstname(firstName){
		this.#firstName = firstName;
	}

	setLastName(lastName){

		this.#lastName = lastName;
	}

	setEmployeeID(employeeID){
		// Additional example: 
		// if(this.#password == undefined){
		// 	throw new Error(
		// 		"You are not authorized to use this function"
		// 	);
		// }
		// else{
		// 	this.#employeeID = employeeID;
		// }

		this.#employeeID = employeeID;
	}


	// Additional Example
	// inputPassword(providedPassword){
	// 	if(providedPassword === "access1001"){
	// 		this.#password = providedPassword;
	// 	}
	// 	else{
	// 		throw new Error(
	// 			"Wrong password"
	// 		);
	// 	}
	// }
}


const employeeA = new Employee('John', 'Smith', 'EM-001');
// Direct access with the field/property firstName will return undefined because the property is private.
// console.log(employeeA.firstName);

// We could not directly change the value of the property firstName because the property is private.
// employeeA.firstName = "David";

console.log(employeeA.getFirstName()); 
employeeA.setFirstname("David");
console.log(employeeA.getFirstName()); 
console.log(employeeA.getFullName());


const employeeB = new Employee();
console.log(employeeB.getFullName()); 

// employeeB.firstName = "Jill";
// employeeB.lastName = "Hill";
// employeeB.employeeID = "EM-002";

employeeB.setFirstname("Jill");
employeeB.setLastName("Hill");
employeeB.setEmployeeID("EM-002");
// console.log(employeeB.firstName);
// employeeB.#firstName = "Jack";
console.log(employeeB.getFullName()); 


// Additional example:
// const employeeC = new Employee();
// // employeeC.password = "access1001";
// employeeC.inputPassword("access1001");
// employeeC.setEmployeeID("EM-003");
// console.log(employeeC.getEmployeeId());
