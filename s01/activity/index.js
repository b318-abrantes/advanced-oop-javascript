class Contractor{
	constructor(name, email, contactNo){
		this.name = name;
		this.email = email;
		this.contactNo = contactNo;
	}
	getContractorDetails(){
		console.log(`Name: ${this.name}`);
		console.log(`Email: ${this.email}`);
		console.log(`Contact No: ${this.contactNo}`);

		// Alternative approach
		// console.log(`Name: ${this.name} \nEmail: ${this.email} \nContact No: ${this.contactNo}`);
		return this;
	}
}

let contractor1 = new Contractor("Ultra Manpower Services", "ultra@manpower.com", "09167890123");
contractor1.getContractorDetails();


class Subcontractor extends Contractor{
	constructor(name, email, contactNo, specializations){
		super(name, email, contactNo);
		this.specializations = specializations;
	}
	getSubConDetails(){
		this.getContractorDetails();
		console.log(`Specializations: ${this.specializations}`);
	}
}

let subCont1 = new Subcontractor("Ace Bros", "acebros@mail.com", "09151234567", ["gardens", "industrial"]);
let subCont2 = new Subcontractor("LitC corp", "litc@mail.com", "09151245678", ["schools", "hospitals", "bakeries", "libraries"]);

subCont1.getSubConDetails();
subCont2.getSubConDetails();
