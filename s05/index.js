/*
	1. Create a Request class that has the following properties:
	requesterEmail (string - email of Employee who created request)
	content (string)
	dateRequested (JS Date object)
*/

class Request{
	constructor(requesterEmail, content, dateRequested){
		this.requesterEmail = requesterEmail;
		this.content = content;
		this.dateRequested = new Date();
	}
}

/*
	2. Create a Person class that is an abstract class
	Check if all classes that inherit the Person class implements following methods:
	getFullName - returns "firstName lastName"
	login - returns "firstName lastName has logged in"
	logout - returns "firstName lastName has logged out"
*/

class Person{
	constructor(){
		if(this.constructor === Person){
			throw new Error(
				"Object cannot be created from an abstract class Person"
			);
		}
		if(this.getFullName === undefined){
			throw new Error(
				"Class must implement getFullName() method."
			);
		}
		if(this.login === undefined){
			throw new Error(
				"Class must implement login() method."
			);
		}
		if(this.logout === undefined){
			throw new Error(
				"Class must implement logout() method."
			);
		}
	}
}

/*
	3. Employee class should use the Person class as blueprint
	Employee Properties (make sure they are private and have getters and setters)
		firstName (string)
		lastName (string)
		email (string)
		department (string)
		isActive (Boolean - default to true)
		requests (array)

	3.B) If the class is Employee, check if it implements the following method:
	addRequest - instantiates a Request object and adds it to the Employee's requests array
*/

class Employee extends Person{
	#firstName;
	#lastName;
	#email;
	#department;
	#isActive;
	#requests;
	constructor(firstName, lastName, email, department) {
		super();
		this.#firstName = firstName;
		this.#lastName = lastName;
		this.#email = email;
		this.#department = department;
		this.#isActive = true;
		this.#requests = [];
	}

	login() {
		return `${this.#firstName} ${this.#lastName} has logged in.`;
	}
	logout() {
		return `${this.#firstName} ${this.#lastName} has logged out`;
	}
	getFullName() {
		return `${this.#firstName} ${this.#lastName}`;
	}
	addRequest(content){
		this.#requests.push(new Request(this.#email, content));
	}

	//	getters
	getFirstName() {
		return this.#firstName;
	}
	getLastName() {
		return this.#lastName;
	}
	getEmail() {
		return this.#email;
	}
	getDepartment() {
		return this.#department;
	}
	getIsActive() {
		return this.#isActive;
	}
	getRequests() {
		return this.#requests;
	}

	//	setters
	setFirstName(firstName) {
		this.#firstName = firstName;
	}
	setLastName(lastName) {
		this.#lastName = lastName;
	}
	setEmail(email) {
		this.#email = email;
	}
	setDepartment(department) {
		this.#department = department;
	}
	setIsActive(isActive) {
		this.#isActive = isActive;
	}
}

// Instantiating an Employee
const employeeA = new Employee();
employeeA.setFirstName("John");
employeeA.setLastName("Smith");
employeeA.setEmail("john@mail.com");
employeeA.setDepartment("IT Department");

// Methods from Employee
console.log(employeeA.login());
console.log(employeeA.getFirstName());
console.log(employeeA.getLastName());
console.log(employeeA.getFullName());
console.log(employeeA.getDepartment());
console.log(employeeA.getEmail());
console.log(employeeA.getIsActive());
console.log(employeeA.logout());

// Adding requests
employeeA.addRequest("Request for CPU repair");
employeeA.addRequest("Request for keyboard replacement");
console.log(employeeA.getRequests());

/*
	4. TeamLead class should use the Person class as blueprint
	TeamLead Properties (make sure they are private and getters and setters)
		firstName (string)
		lastName (string)
		email (string)
		department (string)
		isActive (Boolean - default to true)
		members (array)

	4.B) If the class is TeamLead, check if it implements the following methods:
	addMember - accepts an existing Employee object as an argument and adds it to the TeamLead's members array
	checkRequests - accepts an employee email as an argument, searches for matches in this TeamLead's members array and returns all requests belonging to Employee or a string message if no matches are found
*/

class TeamLead extends Person{
	#firstName;
	#lastName;
	#email;
	#department;
	#isActive;
	#members;
	constructor(firstName, lastName, email, department) {
		super();
		this.#firstName = firstName;
		this.#lastName = lastName;
		this.#email = email;
		this.#department = department;
		this.#isActive = true;
		this.#members = [];
	}

	login() {
		return `${this.#firstName} ${this.#lastName} has logged in.`;
	}
	logout() {
		return `${this.#firstName} ${this.#lastName} has logged out`;
	}
	getFullName() {
		return `${this.#firstName} ${this.#lastName}`;
	}
	addMember(employee) {
		this.#members.push(employee);
	}
	checkRequests(email) {
		const employee = this.#members.find(
			(employee) => employee.getEmail() == email);
		if (employee) {
	  		return employee.getRequests();
	  	} else {
	    	return "Email does not match any existing members.";
	  	}
	}

	//	getters
	getFirstName() {
		return this.#firstName;
	}
	getLastName() {
		return this.#lastName;
	}
	getEmail() {
		return this.#email;
	}
	getDepartment() {
		return this.#department;
	}
	getIsActive() {
		return this.#isActive;
	}
	getMembers() {
		return this.#members;
	}

	//	setters
	setFirstName(firstName) {
		this.#firstName = firstName;
	}
	setLastName(lastName) {
		this.#lastName = lastName;
	}
	setEmail(email) {
		this.#email = email;
	}
	setDepartment(department) {
		this.#department = department;
	}
	setIsActive(isActive) {
		this.#isActive = isActive;
	}
}

// Instantiating a TeamLead
const teamLeadA = new TeamLead();
teamLeadA.setFirstName("Jane");
teamLeadA.setLastName("Doe");
teamLeadA.setEmail("jane@mail.com");
teamLeadA.setDepartment("IT Department")

// Methods from TeamLead
console.log(teamLeadA.login());
console.log(teamLeadA.getFirstName());
console.log(teamLeadA.getLastName());
console.log(teamLeadA.getFullName());
console.log(teamLeadA.getDepartment());
console.log(teamLeadA.getEmail());
console.log(teamLeadA.getIsActive());
console.log(teamLeadA.logout());

// Adding employeeA to members array of teamLeadA
teamLeadA.addMember(employeeA);
console.log(teamLeadA.getMembers());
console.log(teamLeadA.checkRequests("john@mail.com"));
// Checking requests for email that is not a member of teamLeadA
console.log(teamLeadA.checkRequests("jane@mail.com"));


/*
	5. Admin class should use Person class as blueprint
		Admin Properties (make sure they are private and have access methods)
		firstName (string)
		lastName (string)
		email (string)
		department (string)
		teamLeads (array)

	5.B) If the class is Admin, check if it implements the following methods:
	addTeamLead - accepts an existing TeamLead object as an argument and adds it to the Admin's teamLeads array
	deactivateTeam - accepts a team lead email as an argument, searches for a match in the teamLeads array, and sets the isActive property of the team lead and all the team members to false
*/

class Admin extends Person{
	#firstName;
	#lastName;
	#email;
	#department;
	#teamLeads;
	constructor(firstName, lastName, email, department) {
		super();
		this.#firstName = firstName;
		this.#lastName = lastName;
		this.#email = email;
		this.#department = department;
		this.#teamLeads = [];
	}

	login() {
		return `${this.#firstName} ${this.#lastName} has logged in.`;
	}
	logout() {
		return `${this.#firstName} ${this.#lastName} has logged out`;
	}
	getFullName() {
		return `${this.#firstName} ${this.#lastName}`;
	}
	addTeamLead(teamLead) {
		this.#teamLeads.push(teamLead);
	}
	deactivateTeam(email) {
		const teamLead = this.#teamLeads.find(
	    (teamLead) => teamLead.getEmail() == email);
		teamLead.setIsActive(false);
		teamLead.getMembers().forEach((member) => {
	    member.setIsActive(false);
		});
	}

	//	getters
	getFirstName() {
		return this.#firstName;
	}
	getLastName() {
		return this.#lastName;
	}
	getEmail() {
		return this.#email;
	}
	getDepartment() {
		return this.#department;
	}
	getTeamLeads() {
		return this.#teamLeads;
	}

	//   setters
	setFirstName(firstName) {
		this.#firstName = firstName;
	}
	setLastName(lastName) {
		this.#lastName = lastName;
	}
	setEmail(email) {
		this.#email = email;
	}
	setDepartment(department) {
		this.#department = department;
	}
}

// Instantiating an Admin
const adminA = new Admin();
adminA.setFirstName("Admin");
adminA.setLastName("User");
adminA.setEmail("admin@mail.com");
adminA.setDepartment("Admin Department");

// Methods from Admin
console.log(adminA.login());
console.log(adminA.getFirstName());
console.log(adminA.getLastName());
console.log(adminA.getFullName());
console.log(adminA.getDepartment());
console.log(adminA.getEmail());
console.log(adminA.logout());

// Adding an existing teamLead to teamLeads array
adminA.addTeamLead(teamLeadA);
console.log(adminA.getTeamLeads());

// Setting the isActive property of the team lead and members to false
adminA.deactivateTeam("jane@mail.com");
console.log(teamLeadA);
console.log(teamLeadA.getMembers());

// *****